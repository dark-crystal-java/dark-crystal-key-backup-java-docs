#!/bin/bash
cp -r ../dark-crystal-key-backup-message-schemas-java/build/docs/javadoc/* public/dark-crystal-key-backup-message-schemas

cp -r ../dark-crystal-secret-sharing-wrapper/build/docs/javadoc/* public/dark-crystal-secret-sharing-wrapper

cp -r ../dark-crystal-shamir-secret-sharing/java/libsss/build/docs/javadoc/* public/dark-crystal-shamir-secret-sharing

cp -r ../dark-crystal-key-backup-crypto-java/build/docs/javadoc/* public/dark-crystal-key-backup-crypto-java

# cp -r ../dark-crystal-key-backup-java/build/docs/javadoc/* public/dark-crystal-key-backup-java
# cp -r ../confirm-key-java/build/docs/javadoc/* public/confirm-key-java
