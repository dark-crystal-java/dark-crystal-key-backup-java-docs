# Dark Crystal Key Backup API Documentation Index

- [Shamir Secret Sharing](./dark-crystal-shamir-secret-sharing/index.html) JNA bindings to dsprenkles/sss
- [Secret sharing wrapper](./dark-crystal-secret-sharing-wrapper/index.html) - High level functionality for the secret sharing algorithm
- [Key backup crypto](./dark-crystal-key-backup-crypto-java/index.html) - Cryptographic functions
- [Key backup message schemas](./dark-crystal-key-backup-message-schemas/index.html) - Schemas and build methods for the five message types
- [Key backup](./dark-crystal-key-backup-java/index.html) - High level class
- [Confirm-key](./confirm-key-java/index.html) - Derive some dictionary words from a key to help with verbally confirming it with someone
